use anyhow::{Context, Result};
use std::io;
use std::net::{Ipv4Addr, SocketAddrV4, UdpSocket};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::Sender;
use std::sync::{Arc, Barrier};
use std::thread::JoinHandle;
use std::time::Duration;

use socket2::{Domain, Protocol, Socket, Type};

fn new_socket() -> io::Result<Socket> {
    let domain = Domain::ipv4();
    let socket = Socket::new(domain, Type::dgram(), Some(Protocol::udp()))?;
    socket.set_read_timeout(Some(Duration::from_millis(100)))?;
    Ok(socket)
}

fn bind_multicast(socket: &Socket, addr: &SocketAddrV4) -> io::Result<()> {
    socket.bind(&socket2::SockAddr::from(*addr))
}

fn join_multicast(addr: SocketAddrV4) -> io::Result<UdpSocket> {
    let mdns_v4 = addr.ip();
    let socket = new_socket()?;
    socket.join_multicast_v4(mdns_v4, &Ipv4Addr::new(0, 0, 0, 0))?;
    bind_multicast(&socket, &addr)?;
    Ok(socket.into_udp_socket())
}

pub fn multicast_listener(
    tx: Sender<Vec<u8>>,
    running: Arc<AtomicBool>,
    addr: SocketAddrV4,
) -> JoinHandle<Result<()>> {
    let server_barrier = Arc::new(Barrier::new(2));
    let client_barrier = Arc::clone(&server_barrier);

    let join_handle = std::thread::spawn(move || -> Result<()> {
        let listener = join_multicast(addr)?;
        server_barrier.wait();
        while running.load(Ordering::Relaxed) {
            let mut buf = [0u8; 1500];
            match listener.recv_from(&mut buf) {
                Ok((len, _)) => tx.send(buf[..len].to_vec())?,
                Err(err) if err.kind() == std::io::ErrorKind::WouldBlock => (),
                Err(err) => return Err(err).with_context(|| "Listener aborting"),
            }
        }
        Ok(())
    });
    client_barrier.wait();
    join_handle
}

#[cfg(test)]
mod tests {
    use anyhow::Result;
    use socket2::SockAddr;
    use std::net::{Ipv4Addr, SocketAddrV4, UdpSocket};
    use std::sync::atomic::{AtomicBool, Ordering};
    use std::sync::mpsc::channel;
    use std::sync::Arc;
    use std::time::Duration;

    fn new_sender() -> Result<UdpSocket> {
        let socket = super::new_socket()?;
        socket.set_multicast_if_v4(&Ipv4Addr::new(0, 0, 0, 0))?;
        socket.bind(&SockAddr::from(SocketAddrV4::new(
            Ipv4Addr::new(0, 0, 0, 0).into(),
            0,
        )))?;
        Ok(socket.into_udp_socket())
    }

    #[test]
    fn multicast_listener_emits_message() -> Result<()> {
        let (tx, rx) = channel();
        let running = Arc::new(AtomicBool::new(true));
        let addr = SocketAddrV4::new(Ipv4Addr::new(224, 0, 0, 123), 12345);
        let listener = super::multicast_listener(tx, running.clone(), addr);
        let socket = new_sender()?;
        socket.send_to(b"foo", &addr)?;
        let message = rx.recv_timeout(Duration::from_millis(10))?;
        assert_eq!(b"foo", &message[..]);
        running.store(false, Ordering::Relaxed);
        listener.join().unwrap().unwrap();
        Ok(())
    }
}
