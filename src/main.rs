use anyhow::Result;
use signal::trap::Trap;
use signal::Signal;
use std::net::{Ipv4Addr, SocketAddrV4};
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::mpsc::channel;
use std::sync::{atomic::AtomicBool, atomic::Ordering, Arc};
use std::thread;
use std::time::Duration;
use structopt::StructOpt;
use thiserror::Error;

mod multicast;
mod registration;
mod reporting;

#[derive(Debug, PartialEq)]
enum ITxPTVersion {
    V2_0_1,
    V2_1_0,
}

impl FromStr for ITxPTVersion {
    type Err = FMStoIPError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let cleaned = s.replace("v", "").replace(".", "").replace("_", "");
        if cleaned == "20" || cleaned == "201" {
            Ok(ITxPTVersion::V2_0_1)
        } else if cleaned == "21" || cleaned == "210" {
            Ok(ITxPTVersion::V2_1_0)
        } else {
            Err(FMStoIPError::InvalidITxPTVersion(format!(
                "Can't parse version {}. Supported versions are [2.0.1, 2.1.0]",
                s
            )))
        }
    }
}

fn to_duration(input: &str) -> Result<Duration> {
    Ok(Duration::from_millis(input.parse()?))
}

#[derive(StructOpt, Debug)]
#[structopt()]
pub struct Opt {
    /// fmstoip configuration URL (no DNS-SD yet)
    #[structopt(long = "config-url")]
    config_url: String,
    /// fmstoip provision service multicast address (no DNS-SD yet)
    #[structopt(long = "provision-service")]
    provision_service: Ipv4Addr,
    /// fmstoip provision service multicast port
    #[structopt(long = "provision-service-port", default_value = "15015")]
    provision_service_port: u16,
    /// Ask fmrstoip to publish additional PGN
    #[structopt(long = "pgn")]
    pgn: Vec<String>,
    /// ITxPT version for configuration API [2.0.1, 2.1.0]
    #[structopt(long = "itxpt-version", default_value = "2.1.0")]
    itxpt_version: ITxPTVersion,
    // Interval between emitting reports in ms
    #[structopt(long = "report-interval", default_value = "10000", parse(try_from_str = to_duration))]
    report_interval: Duration,
    /// Report path
    #[structopt(long = "output", default_value = "/dev/stdout")]
    output: PathBuf,
}

#[derive(Error, Debug)]
pub enum FMStoIPError {
    #[error("{0}")]
    InvalidITxPTVersion(String),
    #[error("Failed to perform AddPGN: {0}")]
    AddPGNFailure(String),
    #[error("Failed to perform RemovePGN: {0}")]
    RemovePGNFailure(String),
    #[error("Failed to write report to {0}")]
    WriteFailure(#[from] std::io::Error),
    #[error("Reporter failed {0}")]
    ReporterFailure(String),
    #[error("Multicast listener failed {0}")]
    MulticastListenerFailure(String),
}

fn run(
    reporter: thread::JoinHandle<Result<()>>,
    listener: thread::JoinHandle<Result<()>>,
    opt: &Opt,
    blocker: Box<dyn FnOnce() -> Result<()>>,
) -> Result<()> {
    match opt.itxpt_version {
        ITxPTVersion::V2_0_1 => registration::add_pgns_201(&opt)?,
        ITxPTVersion::V2_1_0 => registration::add_pgns_210(&opt)?,
    }
    blocker().unwrap();
    let report_result = reporter.join();
    let listen_result = listener.join();
    match opt.itxpt_version {
        ITxPTVersion::V2_0_1 => registration::remove_pgns_201(&opt)?,
        ITxPTVersion::V2_1_0 => registration::remove_pgns_210(&opt)?,
    }
    report_result.map_err(|err| FMStoIPError::ReporterFailure(format!("{:?}", err)))??;
    listen_result.map_err(|err| FMStoIPError::MulticastListenerFailure(format!("{:?}", err)))??;
    Ok(())
}

fn main() -> Result<()> {
    let opt = Opt::from_args();
    let addr = SocketAddrV4::new(opt.provision_service, opt.provision_service_port);
    let running = Arc::new(AtomicBool::new(true));
    let (tx, rx) = channel();
    let reporter = thread::spawn(reporting::factory(rx, &opt)?);
    let listener = multicast::multicast_listener(tx, running.clone(), addr);
    run(
        reporter,
        listener,
        &opt,
        Box::new(move || {
            if Trap::trap(&[Signal::SIGTERM, Signal::SIGINT]).next().is_some() {
                running.store(false, Ordering::Relaxed);
            }
            Ok(())
        }),
    )
}

#[cfg(test)]
fn opt_from_args(extra: Vec<&str>) -> Opt {
    let config_url = mockito::server_url();
    let mut args = vec![
        "./fmstoip-verify",
        "--config-url",
        &config_url,
        "--provision-service",
        "1.2.3.4",
    ];
    args.extend(extra);
    Opt::from_iter(args)
}

#[cfg(test)]
mod tests {
    use anyhow::Result;
    use mockito::{mock, Mock};
    use std::thread;

    fn api_201() -> (Mock, Mock) {
        (
            mock("GET", "/addpgn").expect(1).with_status(200).create(),
            mock("GET", "/removepgn")
                .expect(1)
                .with_status(200)
                .create(),
        )
    }

    fn api_210() -> (Mock, Mock) {
        (
            mock("POST", "/addpgn").expect(1).with_status(200).create(),
            mock("POST", "/removepgn")
                .expect(1)
                .with_status(200)
                .create(),
        )
    }

    #[test]
    fn parse_itxpt_version_201() {
        let opt = super::opt_from_args(vec!["--itxpt-version", "v2.0.1"]);
        assert_eq!(super::ITxPTVersion::V2_0_1, opt.itxpt_version);
        let opt = super::opt_from_args(vec!["--itxpt-version", "201"]);
        assert_eq!(super::ITxPTVersion::V2_0_1, opt.itxpt_version);
    }

    #[test]
    fn run_uses_itxpt_version_201() -> Result<()> {
        let (addpgn, removepgn) = api_201();
        let opt = crate::opt_from_args(vec!["--pgn", "F001", "--itxpt-version", "2.0.1"]);
        let reporter = thread::spawn(|| -> Result<()> { Ok(()) });
        let listener = thread::spawn(|| -> Result<()> { Ok(()) });
        super::run(reporter, listener, &opt, Box::new(|| Ok(())))?;
        addpgn.assert();
        removepgn.assert();
        Ok(())
    }

    #[test]
    fn run_reporter_panic_still_removes_pgn() -> Result<()> {
        let (addpgn, removepgn) = api_210();
        let opt = crate::opt_from_args(vec!["--pgn", "F001"]);
        let reporter = thread::spawn(|| panic!("boom"));
        let listener = thread::spawn(|| -> Result<()> { Ok(()) });
        let res = super::run(reporter, listener, &opt, Box::new(|| Ok(())));
        addpgn.assert();
        removepgn.assert();
        assert!(res.is_err());
        Ok(())
    }

    #[test]
    fn run_listener_panic_still_removes_pgn() -> Result<()> {
        let (addpgn, removepgn) = api_210();
        let opt = crate::opt_from_args(vec!["--pgn", "F001"]);
        let reporter = thread::spawn(|| -> Result<()> { Ok(()) });
        let listener = thread::spawn(|| panic!("boom"));
        let res = super::run(reporter, listener, &opt, Box::new(|| Ok(())));
        addpgn.assert();
        removepgn.assert();
        assert!(res.is_err());
        Ok(())
    }
}
