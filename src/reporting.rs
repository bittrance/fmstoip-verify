use anyhow::{Context, Result};
use chrono::Utc;
use serde::Deserialize;
use serde_xml_rs::from_str;
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::sync::mpsc::{Receiver, RecvTimeoutError};
use std::time::{Duration, Instant};

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, PartialEq)]
pub struct FMStoIPDelivery {
    FMStoIP: FMStoIP,
    version: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, PartialEq)]
pub struct FMStoIP {
    FMSVersion: String,
    VIN: Option<String>,
    Frame: Vec<Frame>,
}

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, PartialEq)]
pub struct Frame {
    PGN: String,
    Data: String,
    RelativeTime: u64,
    SPN: Option<Vec<SPN>>,
}

#[allow(non_snake_case)]
#[derive(Debug, Deserialize, PartialEq)]
pub struct SPN {
    id: String,
    Name: String,
    Unit: Option<String>,
    Value: Option<String>,
}

fn parse_delivery(delivery: &str) -> Result<FMStoIPDelivery> {
    from_str(delivery).with_context(|| format!("Failed to parse {}", delivery))
}

fn report(stats: &HashMap<String, u64>, mut output: &File) -> Result<()> {
    writeln!(output, "Report {}", Utc::now())?;
    for (pgn, count) in stats {
        writeln!(output, "- PGN {} reported {} times", pgn, count)
            .with_context(|| format!("Failed to write {} entries", stats.len()))?;
    }
    Ok(())
}

pub fn factory(
    rx: Receiver<Vec<u8>>,
    opt: &super::Opt,
) -> Result<Box<dyn FnOnce() -> Result<()> + Send>> {
    let report_interval = opt.report_interval;
    let mut stats = HashMap::new();
    let output = File::create(&opt.output)?;
    let reporter = move || -> Result<()> {
        let mut deadline = Instant::now() + report_interval;
        loop {
            let till_next_report = deadline
                .checked_duration_since(Instant::now())
                .unwrap_or_else(|| Duration::from_micros(1));
            match rx.recv_timeout(till_next_report) {
                Ok(entry) => {
                    let entry = String::from_utf8(entry)?;
                    let entry = parse_delivery(&entry)?;
                    for frame in entry.FMStoIP.Frame {
                        stats.entry(frame.PGN).and_modify(|v| *v += 1).or_insert(1);
                    }
                }
                Err(RecvTimeoutError::Timeout) => {
                    deadline = Instant::now() + report_interval;
                    report(&stats, &output)?;
                    stats.clear();
                }
                Err(RecvTimeoutError::Disconnected) => {
                    report(&stats, &output)?;
                    stats.clear();
                    break;
                }
            }
        }
        Ok(())
    };
    Ok(Box::new(reporter))
}

#[cfg(test)]
mod tests {
    use anyhow::Result;
    use std::sync::mpsc::channel;
    use tempfile::NamedTempFile;

    const DELIVERY_EXAMPLE: &str = std::include_str!("delivery_test.xml");
    const SIMPLE_EXAMPLE: &str = std::include_str!("simple_example.xml");

    #[test]
    fn parse_delivery() {
        let mut delivery = super::parse_delivery(DELIVERY_EXAMPLE).unwrap();
        assert_eq!("VINITXPTSPECV2100".to_owned(), delivery.FMStoIP.VIN.unwrap());
        let frame_0 = delivery.FMStoIP.Frame.get_mut(0).unwrap();
        assert_eq!("FEF1".to_owned(), frame_0.PGN);
        assert_eq!(Some("km/h".to_owned()), frame_0.SPN.take().unwrap().get(0).unwrap().Unit);
    }

    #[test]
    fn parse_delivery_simple() {
        let delivery = super::parse_delivery(SIMPLE_EXAMPLE).unwrap();
        assert_eq!(None, delivery.FMStoIP.VIN);
        let frame_0 = delivery.FMStoIP.Frame.get(0).unwrap();
        assert_eq!("F004".to_owned(), frame_0.PGN);
        assert_eq!("FFFFFFA15AFFFFFF".to_owned(), frame_0.Data);
    }

    #[test]
    fn factory() -> Result<()> {
        let output = NamedTempFile::new()?;
        let opt = crate::opt_from_args(vec!["--output", output.path().to_str().unwrap()]);
        let (tx, rx) = channel();
        let reporter = super::factory(rx, &opt)?;
        tx.send(DELIVERY_EXAMPLE.as_bytes().to_vec())?;
        drop(tx);
        reporter()?;
        let result = String::from_utf8(std::fs::read(output.path())?)?;
        assert!(result.starts_with("Report "), "Result = {}", result);
        assert!(
            result.contains("PGN F004 reported 1 times"),
            "Result = {}",
            result
        );
        Ok(())
    }
}
