use crate::{FMStoIPError, Opt};
use anyhow::Result;

pub fn add_pgns_201(opt: &Opt) -> Result<()> {
    let pgns = opt
        .pgn
        .iter()
        .map(|pgn| format!("<PGNReq>{}</PGNReq>", pgn))
        .collect::<Vec<String>>()
        .join("");
    if pgns.is_empty() {
        Ok(())
    } else {
        let response = ureq::get(&format!("{}/addpgn", &opt.config_url)).send_string(&pgns);
        if response.ok() {
            Ok(())
        } else {
            let message = format!("{:?}", &response);
            Err(FMStoIPError::AddPGNFailure(message).into())
        }
    }
}

pub fn remove_pgns_201(opt: &Opt) -> Result<()> {
    let pgns = opt
        .pgn
        .iter()
        .map(|pgn| format!("<PGNRem>{}</PGNRem>", pgn))
        .collect::<Vec<String>>()
        .join("");
    if pgns.is_empty() {
        Ok(())
    } else {
        let response = ureq::get(&format!("{}/removepgn", &opt.config_url)).send_string(&pgns);
        if response.ok() {
            Ok(())
        } else {
            let message = format!("{:?}", &response);
            Err(FMStoIPError::RemovePGNFailure(message).into())
        }
    }
}

pub fn add_pgns_210(opt: &Opt) -> Result<()> {
    let pgns = opt
        .pgn
        .iter()
        .map(|pgn| {
            format!(
                "<PGN><PGNId>{}</PGNId><UpdatePolicy>EverySecond</UpdatePolicy></PGN>",
                pgn
            )
        })
        .collect::<Vec<String>>();
    if pgns.is_empty() {
        Ok(())
    } else {
        let response = ureq::post(&format!("{}/addpgn", &opt.config_url))
            .send_string(&format!("<AddPGN>{}</AddPGN>", &pgns.join("")));
        if response.ok() {
            Ok(())
        } else {
            let message = format!("{:?}", &response);
            Err(FMStoIPError::AddPGNFailure(message).into())
        }
    }
}

pub fn remove_pgns_210(opt: &Opt) -> Result<()> {
    let pgns = opt
        .pgn
        .iter()
        .map(|pgn| format!("<PGNId>{}</PGNId>", pgn))
        .collect::<Vec<String>>();
    if pgns.is_empty() {
        Ok(())
    } else {
        let response = ureq::post(&format!("{}/removepgn", &opt.config_url))
            .send_string(&format!("<RemovePGN>{}</RemovePGN>", &pgns.join("")));
        if response.ok() {
            Ok(())
        } else {
            let message = format!("{:?}", &response);
            Err(FMStoIPError::RemovePGNFailure(message).into())
        }
    }
}

#[cfg(test)]
mod tests {
    use anyhow::Result;
    use mockito::{mock, Matcher};

    #[test]
    fn add_pgns_201() -> Result<()> {
        let api = mock("GET", "/addpgn")
            .expect(1)
            .match_body(Matcher::Regex(r"^<PGNReq>F001</PGNReq>".to_string()))
            .with_status(200)
            .create();
        let opt = crate::opt_from_args(vec!["--pgn", "F001"]);
        super::add_pgns_201(&opt)?;
        api.assert();
        Ok(())
    }

    #[test]
    fn add_pgns_210() -> Result<()> {
        let api = mock("POST", "/addpgn")
            .expect(1)
            .match_body(Matcher::Regex(
                r"^<AddPGN>.*<PGNId>F001</PGNId>".to_string(),
            ))
            .with_status(200)
            .create();
        let opt = crate::opt_from_args(vec!["--pgn", "F001"]);
        super::add_pgns_210(&opt)?;
        api.assert();
        Ok(())
    }

    #[test]
    fn add_pgn_only_with_pgns() -> Result<()> {
        let api = mock("POST", Matcher::Any)
            .expect(0)
            .with_status(200)
            .create();
        let opt = crate::opt_from_args(vec![]);
        super::add_pgns_210(&opt)?;
        api.assert();
        Ok(())
    }

    #[test]
    fn remove_pgns_201() -> Result<()> {
        let api = mock("GET", "/removepgn")
            .expect(1)
            .match_body(Matcher::Regex(r"^<PGNRem>F001</PGNRem>".to_string()))
            .with_status(200)
            .create();
        let opt = crate::opt_from_args(vec!["--pgn", "F001"]);
        super::remove_pgns_201(&opt)?;
        api.assert();
        Ok(())
    }

    #[test]
    fn remove_pgns_210() -> Result<()> {
        let api = mock("POST", "/removepgn")
            .expect(1)
            .match_body(Matcher::Regex(
                r"<RemovePGN><PGNId>F001</PGNId></RemovePGN>".to_string(),
            ))
            .with_status(200)
            .create();
        let opt = crate::opt_from_args(vec!["--pgn", "F001"]);
        super::remove_pgns_210(&opt)?;
        api.assert();
        Ok(())
    }

    #[test]
    fn remove_pgn_only_with_pgns() -> Result<()> {
        let api = mock("POST", Matcher::Any)
            .expect(0)
            .with_status(200)
            .create();
        let opt = crate::opt_from_args(vec![]);
        super::remove_pgns_210(&opt)?;
        api.assert();
        Ok(())
    }
}
