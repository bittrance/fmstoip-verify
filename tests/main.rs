use anyhow::{Context, Result};
use mockito::mock;
use std::convert::TryInto;
use std::process::{Command, Stdio};
use std::time::Duration;

fn opt_from_args(extra: Vec<String>) -> Vec<String> {
    let config_url = mockito::server_url();
    let mut args = vec![
        "--config-url".to_owned(),
        config_url.to_owned(),
        "--provision-service".to_owned(),
        "224.0.0.1".to_owned(),
    ];
    args.extend(extra);
    args
}

fn with_api_210(test: Box<dyn Fn() -> Result<()>>) -> Result<()> {
    let addpgn = mock("POST", "/addpgn").expect(1).with_status(200).create();
    let removepgn = mock("POST", "/removepgn")
        .expect(1)
        .with_status(200)
        .create();
    test()?;
    addpgn.assert();
    removepgn.assert();
    Ok(())
}

#[test]
fn removes_pgns_on_sigterm() -> Result<()> {
    with_api_210(Box::new(|| {
        let binary = env!("CARGO_BIN_EXE_fmstoip-verify");
        let args = opt_from_args(vec!["--pgn".to_owned(), "F001".to_owned()]);
        let mut child = Command::new(binary)
            .args(args)
            .stdin(Stdio::null())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .spawn()?;
        std::thread::sleep(Duration::from_millis(1000));
        let pid = child.id().try_into()?;
        nix::sys::signal::kill(
            nix::unistd::Pid::from_raw(pid),
            nix::sys::signal::Signal::SIGTERM,
        )?;
        child.wait().map(|_| ()).with_context(|| "Test failed")
    }))
}
