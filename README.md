# fmstoip-verfiy - ITxPT FMStoIP service client

This tool allows you to monitor the FMStoIP (S02P04) multicast data. Optionally, it also allows you to request that the target FMStoIP service report additional [FMS](http://www.fms-standard.com/) messages.

**This is work in progress. In particular, it does not yet support discovery and instead requires that you pass the configuration URL and the UDP multicast IP address and port when invoking the command.**

You can execute is like so:
```bash
./target/debug/fmstoip-verify \
  --itxpt-version 201 \
  --config-url http://192.168.10.1:8085/fmstoip_v201 \
  --provision-service 239.255.42.21 \
  --report-interval 5000 \
  --pgn F003
```

## Shortcomings

The tool currently does not remove PGNs that it has registered.

## Building

You need a modern Rust:
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env
```

You need a C-compiler to build some of the dependencies, e.g.:
```bash
apt-get install gcc
```

With compilers install, you should be able to build the tool:
```bash
cargo build --release
```

The produced binary is in `./target/release/fmstoip-verify`.
